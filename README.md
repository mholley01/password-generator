### README


This cross platform utility allows you to generate random strings using similar options as those provided by online password generation tools, but does not require network connectivity. This tool was created with the fact in mind that it is not always easy or possible to determine if any given online tool stores any of the random strings produced. The application was written in [ObjFPC](https://wiki.freepascal.org/Mode_ObjFPC) using the [Lazarus Component Library and IDE](https://www.lazarus-ide.org/) and made to use the [FPC compiler](https://www.freepascal.org/).

### Screenshots

![](screenshots/gtk-compile.png)![](screenshots/windows-compile.png)



#### Options

The password generator presents the user with a list of options. These include:

- **Uppercase**
- **Lowercase**
- **Numbers**
- **Symbols**
- **Exclude Similar**

Selecting any of the options, with the exception of Exclude Similar, adds the representative character set to the set of characters that may be used to source the randomly generated string. Selecting **Exclude Similar** from the options removes similar looking symbols from the set of characters that is used to source the randomly generated string. The definitions of the sets are as follows:

|Option |Set of Characters |
|---|---|
|Uppercase|{A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z}|
|Lowercase|{a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z}|
|Symbols|{!, @, #, $, %, ^, &, *, (, ), -, _, +, =, [, {, ], }, \, \|, ;, :, ", ', <, ',', ., >, /, ?}|
|Numbers|{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}|
|Similars|{1, l, \|, O, 0, :, ;, I, i}|

It should be noted that selecting a specific character set **does not guarantee** that a symbol will be used from the set. Instead it only allows a character from that set to be randomly selected.


### Compilation

As of writing, there is a compile script for RHEL derivatives that has not been tested except on the original development workstation. More general compilation script(s) will come soon, but are not a priority. Generally, you may load the project in the Lazarus IDE and compile the binary file from there for your platform.


### Licenses

- The FPC Libraries and LCL used in this project adhere to a [modified LGPL](https://wiki.lazarus.freepascal.org/FPC_modified_LGPL). The licenses are available in the third-party-licenses directory.
- The application itself adheres to the GPLv2 available in the root of the repo.
