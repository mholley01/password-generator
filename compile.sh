#!/bin/sh

script_dir="$(cd $(dirname $0) && pwd)"

cd "${script_dir}"

# This removes the warnings due to boilerplate code from the IDE.
lazbuild ./password_generator.lpi \
    | sed -E '/Hint: \([0-9]+\) Parameter "Sender" not used$/d'
