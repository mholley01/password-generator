unit history;

{ Just a Linked List Implementation and Convenience Object }

interface
uses sysutils;

type
  PStrNode = ^StrNode;
  StrNode = record
     data: string;
     next: PStrNode;
  end;
  PTHistory = ^THistory;
  THistory = class
    public
      constructor Create;
      destructor Delete;
      procedure AddString(s: string);
      function AtIndex(i: longword) : string;
      function Size: longword;
    private
      head: PStrNode;
      tail: PStrNode;
      size_: longword;
  end;

function GetHistory: PTHistory;

implementation

var
  H: THistory;

procedure THistory.AddString(s: string);
var
    ToSet: PStrNode;
begin
    if head = nil then
      begin
        new(head);
        ToSet := head;
        tail := head;
      end
    else
      begin
        new(tail^.next);
        ToSet := tail^.next;
        tail := ToSet;
      end;
    ToSet^.data := s;
    ToSet^.next := nil;
    size_ := size_ + 1;
end;

function THistory.Size: longword;
begin
  Size := size_;
end;

function THistory.AtIndex(i: longword): string;
var
  at: longword;
  w: PStrNode;
label e;
begin
  at := 0;
  if head = nil then
    begin
      AtIndex := '';
      goto e;
    end;
  w := head;
  while i <> at do
  begin
    if w^.next = nil then
        break;
    w := w^.next;
    at := at + 1;
  end;
  AtIndex := w^.data;
e:;
end;

constructor THistory.Create;
begin
  head := nil;
  tail := nil;
  size_ := 0;
end;

destructor THistory.Delete;
var
  working : PStrNode;
  d : PStrNode;
begin
  working := head;
  d := working;
  while working <> nil do
  begin
    working := working^.next;
    dispose(d);
    d := working;
  end;
end;

function GetHistory: PTHistory;
begin
  GetHistory := @H;
end;

initialization
  H := THistory.Create;
finalization
  H.Delete;

end.
