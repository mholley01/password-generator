unit main_window;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, Buttons,
  ComCtrls, String_Generation;

type

  { TPrimaryForm }

  TPrimaryForm = class(TForm)
    BtnGenerate: TButton;
    btnCopy: TButton;
    chkExcludeSimilar: TCheckBox;
    chkUppercase: TCheckBox;
    chkLowercase: TCheckBox;
    chkNumbers: TCheckBox;
    chkSymbols: TCheckBox;
    Label1: TLabel;
    trackCharacterCount: TTrackBar;
    TxtCharacterCount: TEdit;
    TxtGeneratedString: TEdit;
    procedure btnCopyClick(Sender: TObject);
    procedure BtnGenerateClick(Sender: TObject);
    procedure chkLowercaseChange(Sender: TObject);
    procedure chkNumbersChange(Sender: TObject);
    procedure chkSymbolsChange(Sender: TObject);
    procedure chkUppercaseChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GenerateStrWithHistory;
    procedure trackCharacterCountChange(Sender: TObject);
    procedure TxtCharacterCountChange(Sender: TObject);
    procedure AllowSimilars;
  private
    GeneratedNum : longword;
  public

  end;

var
  PrimaryForm: TPrimaryForm;

implementation

uses { history, }Clipbrd;
{var
   HistoryObj: PTHistory; }

{$R *.lfm}

{ TPrimaryForm }
procedure TPrimaryForm.AllowSimilars;
var
  Enable: boolean;
begin
  Enable := ((chkUppercase.Checked = true) or (chkLowercase.Checked = true))
            and ((chkNumbers.Checked = true) or (chkSymbols.Checked = true));
  chkExcludeSimilar.Enabled := Enable;
end;

procedure TPrimaryForm.GenerateStrWithHistory;
var
  ReturnString : string;
  GeneratorOpts: GeneratorInput;
begin
  ReturnString := '';
  if TxtCharacterCount.GetTextLen() <> 0 then begin
    with GeneratorOpts do
    begin
      StrLength := StrToInt(TxtCharacterCount.Text);
      UseSymbols := PrimaryForm.chkSymbols.Checked;
      UseNumbers := chkNumbers.Checked;
      UseUppercase := chkUppercase.Checked;
      UseLowercase := chkLowercase.Checked;
      EliminateSimilar :=
        chkExcludeSimilar.Enabled and chkExcludeSimilar.Checked;
    end;
    ReturnString := GenerateString(GeneratorOpts);
    { HistoryObj^.AddString(ReturnString);
      GeneratedNum := HistoryObj^.Size; }
  end;
  TxtGeneratedString.Text := ReturnString;
end;

procedure TPrimaryForm.trackCharacterCountChange(Sender: TObject);
begin
  TxtCharacterCount.Text := IntToSTr(trackCharacterCount.Position);
end;

procedure TPrimaryForm.TxtCharacterCountChange(Sender: TObject);
var
  EnteredValue : integer;
  MaxVal : integer;
  MinVal : integer;
begin
  MinVal := trackCharacterCount.Min;
  MaxVal := trackCharacterCount.Max;
  EnteredValue := MinVal;
  if TxtCharacterCount.GetTextLen > 0 then
  begin
    EnteredValue := StrToInt(TxtCharacterCount.Text);
    if (EnteredValue >= MinVal) and (EnteredValue <= MaxVal) then
      trackCharacterCount.Position := EnteredValue;
  end;
end;

procedure TPrimaryForm.FormCreate(Sender: TObject);
begin
  GeneratedNum := 0;
  GenerateStrWithHistory;
  trackCharacterCount.Position := StrToInt(TxtCharacterCount.Text);
end;

procedure TPrimaryForm.BtnGenerateClick(Sender: TObject);
begin
  GenerateStrWithHistory;
end;

procedure TPrimaryForm.btnCopyClick(Sender: TObject);
var
  C: TClipboard;
begin
  C := Clipboard();
  with C do begin
    Open;
    SetTextBuf(PChar(TxtGeneratedString.Text));
    Close;
  end;
end;

procedure TPrimaryForm.chkLowercaseChange(Sender: TObject);
begin
  AllowSimilars;
end;

procedure TPrimaryForm.chkNumbersChange(Sender: TObject);
begin
  AllowSimilars;
end;

procedure TPrimaryForm.chkSymbolsChange(Sender: TObject);
begin
  AllowSimilars;
end;

procedure TPrimaryForm.chkUppercaseChange(Sender: TObject);
begin
  AllowSimilars;
end;

initialization
  { HistoryObj := GetHistory(); }

end.

