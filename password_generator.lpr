program password_generator;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, main_window
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Title:='Password Generator';
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TPrimaryForm, PrimaryForm);
  Application.Run;
end.

