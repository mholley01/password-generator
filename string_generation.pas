unit string_generation;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

type
  GeneratorInput = record
    StrLength: integer;
    UseUppercase: boolean;
    UseLowercase: boolean;
    UseNumbers: boolean;
    UseSymbols: boolean;
    EliminateSimilar: boolean;
  end;

function GenerateString(Input: GeneratorInput) : String;
function GenerateString(StrLength: integer; UseUppercase, UseLowercase,
                         UseSymbols, UseNumbers,
                         EliminateSimilar: boolean):String;

implementation

const
  LetterCount = 26;
  SymbolCount = 30;
  NumberCount = 10;
  SimilarCount = 9;
  TotalSymbols = LetterCount * 2 + SymbolCount + NumberCount;

  Uppercase: array [0..(LetterCount-1)] of char =
    ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
     'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
  Lowercase: array [0..(LetterCount-1)] of char =
    ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
     'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
  Symbols: array [0..(SymbolCount-1)] of char =
    ('!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-' ,'_', '+',
     '=', '[', '{', ']', '}', '\', '|', ';', ':', '"', '''', '<', ',',
     '.', '>', '/', '?');
  Numbers: array [0..9] of char =
    ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
  Similars: array [0..(SimilarCount-1)] of char =
    ('1', 'l', '|', 'O', '0', ':', ';', 'I', 'i');

procedure FillSymbols(var Arr: array of char;
                      var FillIndex: integer);
var
  i: integer;
begin
  for i:= 0 to SymbolCount - 1 do
  begin
    Arr[FillIndex] := Symbols[i];
    FillIndex := FillIndex + 1;
  end;
end;
procedure FillNumbers(VAR Arr: array of char;
                     VAR FillIndex: integer);
var
  i: integer;
begin
  for i:= 0 to NumberCount - 1 do
  begin
    Arr[FillIndex] := Numbers[i];
    FillIndex := FillIndex + 1;
  end;
end;
procedure FillUcase(VAR Arr: array of char;
                    VAR FillIndex: integer);
var
  i: integer;
begin
  for i:= 0 to LetterCount - 1 do
  begin
    Arr[FillIndex] := Uppercase[i];
    FillIndex := FillIndex + 1;
  end;
end;

procedure FillLcase(VAR Arr: array of char;
                    VAR FillIndex: integer);
var
  i: integer;
begin
  for i:= 0 to LetterCount - 1 do
  begin
    Arr[FillIndex] := Lowercase[i];
    FillIndex := FillIndex + 1;
  end;
end;

procedure RemoveSimilars(var Arr: array of char;
                         var FillIndex: integer);
var
  i: integer;
  j: integer;
  ending: integer;
begin
  ending := FillIndex - 1;
  for i:= 0 to ending do
  begin
    for j := 0 to (SimilarCount-1) do
    begin
      if Arr[i] = Similars[j] then
      { If it is a matching character, move it out of bounds }
      begin
         Arr[i] := Arr[FillIndex - 1];
         FillIndex := FillIndex - 1;
      end;
    end;
  end;
end;

function GenerateString(Input: GeneratorInput): string;
begin
  GenerateString := GenerateString(input.StrLength,
                                   input.UseUppercase,
                                   input.UseLowercase,
                                   input.UseSymbols,
                                   Input.UseNumbers,
                                   Input.EliminateSimilar);
end;

function GenerateString(StrLength: integer; UseUppercase, UseLowercase,
                         UseSymbols, UseNumbers,
                         EliminateSimilar: boolean):String;
var
  Generated: String;
  NewSetAmount: integer;
  FillIndex: integer;
  FlattenedArray: array [0..TotalSymbols-1] of char;
  RandomNumber: real;
label
  return;
begin
  RandomNumber := 0.0;
  NewSetAmount := 0;
  Generated := '';
  for FillIndex := 0 to TotalSymbols-1 do
      FlattenedArray[FillIndex] := ' ';
  FillIndex := 0;
  if UseLowercase = true then
     FillLcase(FlattenedArray, FillIndex);
  if UseUppercase = true then
     FillUCase(FlattenedArray, FillIndex);
  if UseSymbols = true then
     FillSymbols(FlattenedArray, FillIndex);
  if UseNumbers = true then
     FillNumbers(FlattenedArray, FillIndex);
  if EliminateSimilar = true then
     RemoveSimilars(FlattenedArray, FillIndex);
  Randomize();
  NewSetAmount := FillIndex - 1;
  FillIndex := 0;
  if NewSetAmount = 0 then
     goto return;
  While (FillIndex < StrLength) do
  begin
    RandomNumber := random() * (NewSetAmount);
    Generated := Generated+FlattenedArray[round(RandomNumber)];
    FillIndex := FillIndex + 1;
  end;
  GenerateString := Generated;
return:;
end;


end.

